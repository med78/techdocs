# Zabbix Server Setup and Configuration

## First-time TTW setup

- After running playbook(s) to provision server, go to host URL in browser
- Requirements should be OK - Next
- Database connection - enter password, other settings use defaults
- Zabbix server details - Enter host name (replace localhost), port is default, Name: "Monitor [DEV|PROD]"

Login as Admin/zabbix, change password to vaulted password, and proceed to GUI config below.

## GUI Config

### Top tab: Administration

- Create admin accounts for sysadmins

```
Sub-tab: Users
  - Create user
    - User form
      Alias: NetID (unqualified)
      Name: First name
      Last Nam: Last name
      Groups: Zabbix administrators
      Password/Confirmation: [generate random, e.g. with pwgen, won't be used]
      Language: English (en_US)
      (skip other field, but don't submit yet ...)
    - Permissions form
      User type: Zabbix Super Admin
      Submit: Add
```

- Enable "HTTP" authentication for Shib integration

```
Sub-tab: Authentication
  - HTTP Settings
    Enable HTTP authentication
    Remove domain name: duke.edu
(After setting this, you should be able to login with HTTP)
```

- Enable automatic inventory as the default host setting

```
Sub-tab: General
  - Dropdown: Other
    - Default host inventory mode: Automatic
```

- Email config

```
Sub-tab: Media Types
  - Email
    - SMTP Server: smtp.duke.edu
    - SMTP helo: duke.edu
    - SMTP email: zabbix@<host name>
```

### Top tab: Configuration

- Enable auto-discovery

```
Sub-tab: Discovery
  - Create Discovery Rule
    Name: Internal network
    IP Range: 152.3.9.192/26
    Checks: Zabbix agent / Key: system.uname

Sub-tab: Actions
  - Auto discovery. Linux servers.
    Enabled
    Conditions:
      Service type: Zabbix agent
```
