# DDR: Cache Management

Both ddr-admin and ddr-xenon implement caches served by Memcached.

## Clearing the Cache

One way to clear the cache is simply to restart the `cache` service.

[This example uses the public application production server.  To accomplish this 
task on the staff application, substitute `ddr-admin` for `ddr-xenon` everywhere
below.]

Make sure you are connected to the VPN, then SSH to the server:

    $ ssh ddr-xenon.lib.duke.edu

(On Windows, use a GUI terminal application like PuTTY.)

You will be prompted for your NetID password and multi-factor authentication.

Change to the project directory:

    [ddr-xenon]$ cd /opt/docker-compose/projects/ddr-xenon

Restart the cache service:

    [ddr-xenon]$ sudo docker-compose restart cache

Logout of the server.

