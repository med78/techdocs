# DDR Architecture

The DDR consists of four (4) application stacks bound together by web services.
Except for ddr-helium (Apache + Solr), each stack is coordinated by docker-compose 
and supervised by systemd.

## ddr-admin: The Staff UI

The administrative "staff" application for managing the content of the repository.

It consists of the follow services:

- app: The Rails application based on Valkyrie and Blacklight.
- resque: The background job processing service, based on the app container image.
- redis: The Redis server backend for resque.
- db: The PostgreSQL database (Valkrie/ActiveRecord)
- web: Apache HTTPD 2.4 server
- shib: Shibboleth service provider
- clamav: ClamAV antivirus daemon (clamd); the app and resque service run the `clamdscan` client.
- cache: Memcached service 

Two services are present for the migration phase only and will be subsequently removed:
- mysql: MySQL service for migration of MySQL data
- pgloader: Service for migrating MySQL data to PostgreSQL