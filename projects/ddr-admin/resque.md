# ddr-admin: Resque

[Resque](https://github.com/resque/resque) is the "backend" we use for job processing in the production environment
(i.e., all the servers).  Resque uses Redis as a datastore for queues, workers and jobs.

In order to manage multiple queues, each having one or more dedicated
workers, we use [resque-pool](https://github.com/nevans/resque-pool).

The `resque` service in the docker-compose configuration starts a resque-pool
manager which spawns processes for the workers.

To restart the queue manager:

    $ docker-compose restart resque
    
    