# Wiping Morphosource demo data

```
sudo systemctl stop tomcat solr rails
cd /nas/morphosource_demo
sudo mv derivatives fcrepo.binary.directory backup/
sudo rm -rf /var/solr/data/hyrax/data
sudo -u postgres scl enable rh-postgresql95 -- psql -c "drop database fcrepo"

sudo -u hydra -i
cd /opt/morphosource/root
DISABLE_DATABASE_ENVIRONMENT_CHECK=1 bundle exec rake db:reset 
[exit]
```

After: Re-run `morphosource.yml` playbook