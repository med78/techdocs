# OpenShift Environment for DUL-ITS
[[_TOC_]]
## Projects Currently Using OKD

#### DUL-QUICKSEARCH
Repo URL: https://gitlab.oit.duke.edu/dul-its/dul-quicksearch  

**Deployment Method -- Template**  
OKD template is used to deploy the "quicksearch-web" and "postgresql" (DB) containers.
  
**Webhook**  
Includes "Webhook" used to trigger builds on updates to selected branch (currently "openshift" branch).  
  
#### DUL-HUGINN
https://gitlab.oit.duke.edu/dul-its/dul-huginn  

## Building Custom Images / Pushing to OKD Project

These commands were used to push a customized Perl Dancer2 image to an OKD imagestream.  
  
```sh
$ cd /path/to/project
$ oc login ... (obtain login command from OKD console)
$ sudo docker login -u openshift -p $(oc whoami -t) registry.cloud.duke.edu
$ sudo docker build -t <mytag> .
$ sudo docker tag <mytag> registry.cloud.duke.edu/<project-name>/<mytag>:latest
$ sudo docker push registry.cloud.duke.edu/<project-name>/<mytag>:latest
```

### Using DUL Domains on Duke's OKD
Simple point a CNAME to `os-node-lb-fitz.oit.duke.edu`. Routes can then be created using the hostname.
  
Then, exposing a service -- such as:  
`oc expose svc/<my-app> --hostname=<my-dul-hostname>`
  
...will allow us to use http://my-dul-hostname in a web browser (or https://my-dul-hostname).
  
### Integrating NFS Volumes
Nate Childers (OIT) can configure a PersistentVolume for our NFS shares.

#### Exporting NFS Shares to Duke's OpenShift Cluster
Add `10.138.5.0/24` to the list of target servers.
  
### Persistent Volume Claim YAML Example
**volume-lib-asset-share** is a PersistentVolume configured by Nate Childers, using our oit-nas-fe11.oit.duke.edu:/LIB-ASSETS-SHARE NFS share.  
  
**Adding a PersistentVolumeClaim** is as easy as this:
```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: volume-lib-assets-share
spec:
  accessModes:
  - ReadWriteMany
  resources:
    requests:
      storage: 1Ti
  storageClassName: standard
  volumeName: volume-dul-sandbox-lib-assets-share
status: {}
```
It's best to create a template file and `oc apply -f <your-template>.yml`
