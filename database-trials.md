# Adding Staff to Database Trials Web App
**Database Trials** is a legacy database application used by selected Library Staff to explore potential "database" resources prior to purchase.  
Or at least, that's how I understand it.  
## Website URL
http://dbtrials.library.duke.edu/  
  
While portions of the website are open to the public, access to certain portions are Shibboleth-controlled. Staff who belong to the below group are granted access:  
`urn:mace:duke.edu:groups:library:applications:cms:dbtrials`

## Request for Access
Requests for access to the website normally come through Service Now.  
