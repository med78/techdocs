# The DevOps Plan
```sh
% which plan
% /usr/bin/which: no plan in (anywhere in this documentation)
```

### Preamble
An attempt to communicate the ideas and goals of ongoing work by the ITS DevOps team, and to (loosely) present a roadmap to the near future.

## Virtual Machines and OIT

### Background
ITS Core Services has been a primary consumer of OIT's "OIT-Managed VM" offerings for the last 4 years.  These VMs are used to host 
various services including, but not limited to, the [Libraries Public Website](https://library.duke.edu) and our Research Data Repository.  
  
Over time, it has become clear that the services provided by OIT (installation and support) are best suited for non-Enterprise level applications, such as our Drupal 
instance which powers the Libraries Public website.  To that end, OIT has been a very good technical partner for us.  
  
It has also become clear that Enterprise-level applications, such as the RDR, present a "support" challenge and appear to 
fall beyond the scope of what OIT can support in their "OIT-Managed" model.  
  
**Automated tasks**, such as installing Solr from an Ansible playbook, expose "DevOps workflow" roadblocks that stretch the limits of the "OIT-Managed" 
support model.
  
As the DevOps team is seeking to implement automation across as many hosts as possible, we have determined that the "OIT-Managed" VM 
offering has limits and presents support roadblocks to this newly emerging workflow.  
  
### Self-Managed VM Exploration
In recent months, members of the ITS DevOps team have explored and/or piloted ideas centered around using OIT's "Self-Managed VM" offerings 
non-critical applications and/or services.  
  
For instance, we have successfully deployed a self-managed VM used in the Data and Visualization Services GeoBlacklight Pilot Project.  
We have identified other applications and services in which we are exploring the uses of Self-Managed VMs from OIT.  
  
Using these VMs come with the benefit of cost savings, while presenting a risk of support expectations.

## ROADMAP
- **Identify Non-Critical Applications/Services** that can be hosted on self-managed services with "8x5, 5 day" service expectations.
- **Bootstrapping Process** - explore technologies that minimize the number of manual steps needed to provision a new VM.
  
