# Derrek's Perl Primer for ITS Devs/DevOps

[[_TOC_]]
## Perl is not scary / Perl is your friend
Despite what you may (or may not have heard), Perl is not a scary language. On the contrary, it is well-suited for both backend scripting and web application development.  
## DUL Applications/Projects Using Perl
There are several Perl-based web applications in DUL's portfolio.  

* [**Requests App**](https://gitlab.oit.duke.edu/dul-its/crs) @ https://requests.library.duke.edu  
  The current "Item Request" and "My Accounts" workflows are supported by this web application.

* [**DUL Naming Opportunites**](https://gitlab.oit.duke.edu/dul-its/floorplans) @ https://naming.library.duke.edu  
  The public UI used by Library Development for Fund Raising.

* [**EZParser Dashboard**](https://gitlab.oit.duke.edu/dul-its/ezparser) @ https://ezparser.lib.duke.edu  
  Internal web app that displays EZproxy usage by Staff, Students, Affiliates, etc.

* [**Location Guide/Finder Management**](https://gitlab.oit.duke.edu/dul-its/location-finder) (in development)  
  This web app replaces a legacy Django implementation, and is intended for use by DUL Circulation Staff.  
  ***Note:*** The README for this project includes very useful "Getting Started" information.
  
### About The Projects / Dancer2
Each project is written on top of the [Dancer2 application framework](https://metacpan.org/pod/distribution/Dancer2/lib/Dancer2/Manual.pod) -- selected because of its similarity to Rails. 
## Understanding Perl
### Top of a Typical Perl Script
```perl
#!/usr/bin/perl  <-- or wherever your Perl is installed

# ensure code safety/integrity
use strict;

# allow Perl to warn you of potential bad stuff
use warnings;
```
### Variables (Scalars)
Perl is an untyped language, meaning you can assign a value to a variable, and the interpreter 
will discern the intended type. So...
```perl
my $a_var = 1;
```
  
The intepreter will determine that `$a_var` is an integer scalar.  
***Note:*** Because we're using the `use strict;` declaration, you'll need to specify `my ...` when 
declaring variables.  
  
This will help you remember the scope of your variables.
### Variables (Arrays)
#### Declaring an array
```perl
# your first array is empty
my @first_array = ();

# your second one has items...
my @second_array = ('Duke', 'NCCU', 'UNC', 'NC State');
```
Declarations of arrays begin with the `@` symbol -- pretty easy to remember, right?
#### Accessing Array Items
Now you have an array to work with (or play with), so...
```perl
# you declare the array with the '@' symbol.
# when it's time to access one of the items...
my $second_item = $second_array[1]; # NCCU

# or if you want to replace an item...
$second_array[2] = 'UNC Sucks!'; # self-explanatory :)
```
### Variables (Hashes)
Perl hashes, similar to Ruby, are the workhorse of data organization in Perl.  
  
When declaring a hash, the `%` is used -- again, easy to remember, right?
```perl
my %conferences = (
    'acc' => 'The best',
    'sec' => 'The not-so-best',
);
```

## Moving On
Once this makes sense, you're ready to move on to the next section:  
[Variables - Real World Usage](./vars-advanced.md)
