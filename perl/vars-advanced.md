# Variables (Real World Usage)
[[_TOC_]]
## Variable References (Arrays, Hashes)
In the previous bit, you learned to create simple arrays and hashes.  
  
In most real-world projects, arrays and hashes are managed using references. 
From [*Tutorialspoint.com*](https://www.tutorialspoint.com/perl/perl_references.htm):
> A Perl reference is a scalar data type that holds the location of another value which could be scalar, arrays, or hashes.
  
## Real World Examples
### Array References
Similar to PHP and JSON, array references are declared using `[]`:
```perl
my $array_ref = ['Thriller', 'Billie Jean', 'Rock With You',];
```
  
...or, you can assign an array as a reference:
```perl
my @favorite_bands = ('Genesis', 'Chicago', 'The Commodores', 'The Spinners');
my $favorites = \@favorite_bands;

# or, the one-line approach:
my $favorite_bands = \(
    'Genesis',
    'Chicago',
    'The Commodores',
    'The Spinners',
);
```
#### Accessing Array Data from ARRAYREFs
```perl
# Take note of the -> (arrow) symbol
my $third_item = $favorite_bands->[2];
```

### Hash References
Similar to PHP and JSON, hash references are declared using `{}`:
```perl
my $hash_ref = {
    'foo' => 'bar',
    'yin' => 'yang',
    'ping' => 'pong',
};
```
  
...or:
```perl
my %acc_divisions = (
    'atlantic' => ('Boston College', 'Clemson', 'FSU', 'Louisville', 'NC State', 'Syracuse', 'Wake Forest',),
    'coastal' => ('Duke', 'Georgia Tech', 'Miami', 'North Carolina', 'Pitt', 'Virginia', 'Virginia Tech'),
);
my $acc = \%acc_divisions;
```
  
But in a really, "REAL WORLD" script, all of that would be a reference:
```perl
my $acc_divisions = {
    'atlantic' => [...],
    'coastal' => [...],
};
```
#### Accessing Items from HASHREFs
```perl
my $coastal_arrayref = $acc->{'coastal'};

# de-referencing the array
my @coastal_array = @{ $acc->{'coastal'} };
```
## Useful Resource Links
[Tutorialspoint.com on Perl References](https://www.tutorialspoint.com/perl/perl_references.htm)  
The page's sidebar nav has some great chapters on all aspects of Perl.
