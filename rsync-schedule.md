# Nightly Sync Jobs

## XFER-01

**rdr to rdr-archive**  
12:01a Daily  

**ddr to ddr-archive**  
1:00a Daily  

## XFER-02
  
**dspace to dspace-archive**  
12:30a Mon, Wed, Fri  

## XFER-03

**dpc to dpc-archive**  
12:15a Sun, Tue, Thu  
  
**rubenstein-archive to rubenstein-copy**  
12:15a Mon, Wed, Fri