# Troubleshooting

## Logstash Error: Retrying Failed Action With Response Code 403

This condition seems to occur when ElasticSearch encounters a disk full error.  
  
Inspect the `index.blocks.read_only_allow_delete` property for a given ElasticSearch index.  
  
For this example, we'll use **xfer-02-rsync-2020.11.04**:
```bash
[dlc32@event-logging ~]$ curl -s -X GET "http://localhost:9200/xfer-02-rsync-2020.11.04/_settings"
{"xfer-02-rsync-2020.11.04":{"settings":{"index":{"number_of_shards":"1","blocks":{"read_only_allow_delete":"true"},"provided_name":"xfer-02-rsync-2020.11.04","creation_date":"1604448000070","number_of_replicas":"1","uuid":"O8UgbO4nRiuXkSE-NeWBrQ","version":{"created":"7030199"}}
```

Note that `read_only_allow_delete` is true. In order to un-clog the index, we'll need this property to be removed:  
```bash
curl -s -X PUT -H "Content-Type: application/json" -d '{"index.blocks.read_only_allow_delete": null}' "http://localhost:9200/xfer-*/_settings"
```
This effectively un-clogs all indexes prefixed with "xfer-".
