# Git Hooks

See https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks.

**pre-commit hook to abort if unencrypted key(s) found**

Save to `.git/hooks/pre-commit` in a git repository.

```sh
#!/bin/bash                                                                                                                                                     
if git ls-files -c -m -o --exclude-standard | xargs grep -E 'BEGIN (.*)?PRIVATE KEY'; then
    echo -e "\nAborting commit due to unencrypted keys!"
    exit 1
fi
```