# Clone DevOps Playbooks Project
[[_TOC_]]

With [ansible installed on your local environment](./install-ansible.md), you're ready to clone the DevOps Playbook project:
```sh
cd /path/to/workspace
git@gitlab.oit.duke.edu:devops/antsy.git
```
**This step assumes your SSH or GPG key has loaded to your GitLab profile.**

## Next Step(s)

### Update Your PATH
Within the "antsy" playbook project, we have a `bin/` directory containing useful wrapper scripts.  
Simply update your `PATH` to include `/path/to/workspace/antsy/bin`. This will make it easy to use the wrapper scripts.
  
## Drupal9 Tasks
Drupal developers can easily rebuild the cache on production, staging or development without needing to SSH into the actual VMs.  

### Rebuild Cache
```sh
rebuild-drup-cache.sh [production|staging|development]
# the default environment setting is "development"
```

### Add/Remove Packages (Modules/Themes)
```bash
drup-require-pkg.sh [-i <production|staging|development>] pkg1 [pkg2 ... pkgN]
drup-remove-pkg.sh [-i <production|staging|development>] pkg1 [pkg2 ... pkgN]
# 'development' is the default inventory when -i is omitted
```

### Enable Modules
```bash
en-drup-mod.sh [-i <production|staging|development>] mod1 [mod2 .. modN]
```

### Uninstall Components
Coming soon
