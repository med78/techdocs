# Installing Ansible
[[_TOC_]]

## Installing Ansible Locally

Installing ansible locally to your workstation is simple:  
* https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

There are sections for installing to macOS, CentOS, Ubuntu, etc.

## Additional Setup
When running the DevOps' playbook environment, you'll need to install the `pyldap` Python module. Some playbooks require this module, others do not -- but it's safe to have it and avoid potentially annoying errors.

### Python2 Installation (Ubuntu example)
```bash
sudo apt-get install python-dev libldap2-dev libsasl2-dev libssl-dev
sudo pip install python-ldap
```

### Python3 Installation (Ubuntu example)
```bash
sudo apt-get install python-dev libldap2-dev libsasl2-dev libssl-dev
sudo pip3 install python-ldap
```

## Configure Ansible Locally
```bash
cd /home/directory
wget https://automation.lib.duke.edu/ansible/ansible-settings.tar.gz
tar xzvf ansible-settings.tar.gz

# .ansible/ and .ansible.cfg will be under /home/directory
```
  
Inspecting `.ansible.cfg`:
```bash
[defaults]
executable = /bin/bash
remote_user = libautomation
vault_password_file = ~/.ansible/vault-password
private_key_file = ~/.ansible/libautomation_key.pem
log_path = ~/.ansible/ansible.log
retry_files_enabled = False
host_key_checking = False
timeout=30
sudo_flags=-H -S
```
Other than the logging location, you shouldn't need to adjust any of these settings.  

### Modify file permissions (for Mac users)
- cd into the .ansible folder
- we need to remove the executable bit from vault-password by running `chmod -x vault-password`
- we also need to change the permissions of libautomation_key.pem to only be readable by you by running `chmod 400 libautomation_key.pem`
  
## All Set!
Now, you're ready to run playbooks from the [DevOps Playbook Project](./clone-playbooks.md)!