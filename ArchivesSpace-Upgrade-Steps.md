### Switch to 'aspace' user
`sudo -u aspace -i`

### Cloning ArchivesSpace projecct to /srv/perkins/apps
`git clone <project-repo>.git /srv/perkins/apps/archivesspace-v<newvers>`  
  
### Recreate Symbolic Link
```sh
cd /srv/perkins/apps
rm archivesspace
ln -s /srv/perkins/apps/archivesspace-v<newvers> /srv/perkins/apps/archivesspace
```
  
### Copy MySQL Connector Library
Assuming the current installed version of ArchivesSpace resides in /srv/perkins/apps/archivesspace-v2.2.0: 
```sh
cp /srv/perkins/apps/archivesspace-v2.2.0/lib/mysql-connector-java-5.1.39.jar /srv/perkins/apps/archivesspace-v<newvers>/lib
```

### Inspect and Copy sections of config/config.rb
It's important not to copy and replace this file, but rather sections of the previous file that address:
* LDAP
* Solr
* Ports

Looking in the config.rb file, copy these settings (but not limited to):
`AppConfig[:db_url]`
`AppConfig[:backend_url]`
`AppConfig[:solr_url]`
`AppConfig[:plugins]`
`AppConfig[:public_formats_resource_links]`
`AppConfig[:public_formats_digital_object_links]`
`AppConfig[:user_defined_in_basic]`
`AppConfig[:frontend_proxy_url]`
`AppConfig[:authentication_sources]`