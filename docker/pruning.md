# Docker: Pruning 

To list "dangling volumes":

    $ docker volume ls -f 'dangling=true'

In order to delete "dangling volumes", run this command:

    $ docker volume prune
    
To clean up all docker data (CAUTION!):

    $ docker system prune -a -f --volumes
    
To delete dangling images:

    $ docker image prune