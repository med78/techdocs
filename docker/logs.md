# Docker logs

Where possible, we configure container services to log to stdout.  In a docker-compose context, these logs are availble 
using the `docker-compose logs` command -- see https://docs.docker.com/compose/reference/logs/ for more information.
Using this method you can view or tail the logs for a particular service, e.g.:

    $ docker-compose logs -f app

In addition, the docker daemon is configured to send logs to the systemd journal (journald).  To view those logs, use
the `journalctl -u docker [OPTIONS]` command.  See `man journalctl` for more information about the available options.
the systemd logs should be available for up to 30 days before rotating out of circulation.