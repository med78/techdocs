# Docker stats notes

`docker stats` has a `--format` argument that takes a 
[Go template](https://golang.org/pkg/text/template/) string.

See https://docs.docker.com/engine/reference/commandline/stats/#formatting.

Fields:

```
.Container	Container name or ID (user input)
.Name		Container name
.ID		Container ID
.CPUPerc	CPU percentage
.MemUsage	Memory usage
.NetIO		Network IO
.BlockIO	Block IO
.MemPerc	Memory percentage (Not available on Windows)
.PIDs		Number of PIDs (Not available on Windows)
```

Example:

```
$ docker stats --format '{{.Name}},{{ index (split .MemUsage " / ") 0 }}' --no-stream
ddr-admin_app_1,179.2MiB
ddr-admin_resque_1,7.021GiB
ddr-admin_web_1,8.879MiB
ddr-admin_db_1,13.91MiB
ddr-admin_redis_1,26.61MiB
ddr-admin_cache_1,1.801MiB
ddr-admin_mysql_1,25.63MiB
ddr-admin_shib_1,5.758MiB
ddr-admin_clamav_1,90.79MiB
```