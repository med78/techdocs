# Hotfixing a file in a running container - Example

### Example: Pre-conditions

- docker-compose project, with `COMPOSE_FILE` set or `docker-compose.yml` in working directory.
- Rails app service named `app`.
- Rails app container path is `/usr/src/app`.
- Need to patch `User` model class at relative path `app/models/user.rb`

### Example: Procedure

Check the name of the running container:

    $ docker-compose ps

    Name                      Command               State                     Ports                  
    ---------------------------------------------------------------------------------------
    ddr-admin_app_1        docker-entrypoint.sh bundl ...   Up       0.0.0.0:3000->3000/tcp                  

(condensed output)

Copy the file from the container to the host using the Docker `cp` command:

    $ docker cp ddr-admin_app_1:/usr/src/app/app/models/user.rb /tmp

(See https://docs.docker.com/engine/reference/commandline/cp/)

Edit the file as desired (the host system should at least have `vi`, or you can 
scp or rsync to your workstation).

Copy the edited file back into the container:

    $ docker cp /tmp/user.rb ddr-admin_app_1:/usr/src/app/app/models/user.rb
    
Restart rails:

    $ docker-compose exec app bundle exec rails restart
    
(Note that we are not restarting the *container*, but the Rails process inside it.)


