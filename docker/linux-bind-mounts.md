# Docker: Notes on Linux bind mounts

In development, it is useful to bind mount the source code into the application container(s). 
Unfortunately, this can lead to permissions issue, in particular with Rails apps that want to
write to `tmp/` and `log/`. A solution is to `chgrp` those directories to the gid of the 
container user -- in our case, typically `1001`.

    $ sudo chgrp 1001 tmp log

These directories are probably group writable, but if not, add `chmod g+w tmp log`.
