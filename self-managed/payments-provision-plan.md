# Deployment Plan for "payments.library.duke.edu"

In response to recent ITSO security scans facilitated by OIT, the website located at "payments.library.duke.edu" 
has been flagged for the following items:
* Insecure PHP version
* Presence of TLSv2 in web server
  
In response to this, the DevOps team has elected to move this application service to its own Docker container 
running on a lightweight self-managed VM.
  
This document details the plan for migrating the production environment from its current home on 
`libweb-01` to a new lightweight self-managed VM.

### Remove 'payments.library.duke.edu' CNAME DNS Entry
Currently, the host is a CNAME alias that is linked to "library.duke.edu".  This will need to be removed 
in order to use this host for a new self-managed VM.
  
### Create Self-Managed VM for "payments.library.duke.edu"
We will create a new VM with "payments.library.duke.edu" as the host name.  This VM will be lightweight (4 GB RAM, 2 CPUs), and will have 
minimal OS software installed -- just the bare minimum to support Docker and GitLab Runner.

### Trigger GitLab CI Pipeline Job
The GitLab project for this web application has been configured to support "continuous integration" such that 
commits to the repository's `master` branch will trigger a deployment job.  The newly created VM will have a registered "runner" that 
will respond to this event and (re)build the Docker container that contains the codebase.

## About The Docker Image
The Docker image that will be used for serving the web application is based on the `5.6.38-apache-jessie` image hosted on **hub.docker.com**.  
PHP 5.6.38 represents the minimal PHP 5 version that satisfies the ITSO security requirements.

## Proposed Deployment Date
This deployment process is scheduled for **Friday, December 7, 2018**.