# Common UIDs/GIDs

| User | UID | GID |
|------|-----|-----|
| apache | 48 | 48 |
| postgres | 26 | 26 |
| tomcat | 53 | 53 |